package logic

case class Point[T](x: T,
                    y: T)