package logic

private class UpdateTimer(val updatesPerSecond: Double) {
    val updateDurationInMs: Double  = 1000 / (if (updatesPerSecond > 0) updatesPerSecond else 0.0001d)
    val nextUpdateStartTime: Double = System.currentTimeMillis() + updateDurationInMs

    def timeForNextUpdate(): Boolean = System.currentTimeMillis() >= nextUpdateStartTime
    def update(): UpdateTimer        = new UpdateTimer(updatesPerSecond)
}