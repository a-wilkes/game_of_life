package logic

case class Cell(location: Point[Int]) {
    // TODO: add degrading age of cell since activation/death

    def isNeighbourOf(p: Point[Int]): Boolean =
        Vector.range(p.x - 1, p.x + 2).contains(location.x) &&
            Vector.range(p.y - 1, p.y + 2).contains(location.y)

    def canEqual(a: Any): Boolean = a.isInstanceOf[Cell]

    override def equals(that: Any): Boolean =
        that match {
            case that: Cell =>
                that.canEqual(this) &&
                this.location == that.location
            case _ => false
        }

    override def hashCode: Int = location.hashCode()
}