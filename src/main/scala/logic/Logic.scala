package logic

class Logic(private val timer: UpdateTimer,
            private val dimensions: Point[Int],
            private val activeCells: Set[Cell],
            val livingCells: Set[Cell]) {

    def this(updatesPerSecond: Float, d: Point[Int]) =
        this(new UpdateTimer(updatesPerSecond), d, Set(), Set())

    def createCellAt(p: Point[Int]): Logic  = changeCellAt(p, livingCells + Cell(p))
    def destroyCellAt(p: Point[Int]): Logic = changeCellAt(p, livingCells - Cell(p))

    private def changeCellAt(p: Point[Int], l: Set[Cell]): Logic =
        createLogic(activeCells ++ getNeighbourhood(p), l)

    def increaseUpdateSpeed(): Logic = createLogic(timer.updatesPerSecond + 1)
    def decreaseUpdateSpeed(): Logic = createLogic(timer.updatesPerSecond - 1)

    def update(): Logic =
        if (!timer.timeForNextUpdate()) this
        else createLogic(timer.update(), nextActiveCells(), nextLivingCells())

    private def nextLivingCells(): Set[Cell] =
        activeCells.filter(c => willLive(c)).union(livingCells.filter(c => willLive(c)))

    private def willLive(c: Cell): Boolean =
        countActiveNeighbours(c) match {
            case 3           => true
            case n if n != 4 => false
            case _           => livingCells.contains(c)
        }

    private def nextActiveCells(): Set[Cell] =
        changedCells().flatMap(c => getNeighbourhood(c))

    private def changedCells(): Set[Cell] = {
        val survivors = livingCells.intersect(nextLivingCells())
        val births = nextLivingCells().diff(survivors)
        val deaths = livingCells.diff(survivors)
        births.union(deaths)
    }

    private def countActiveNeighbours(c: Cell): Int =
        livingCells.count(d => d.isNeighbourOf(c.location))

    private def getNeighbourhood(c: Cell): Set[Cell] =
        getNeighbourhood(c.location)

    private def getNeighbourhood(p: Point[Int]): Set[Cell] = {
        var n: Set[Cell] = Set()
        for (x <- Math.max(0, p.x - 1) to Math.min(dimensions.x, p.x + 1))
            for (y <- Math.max(0, p.y - 1) to Math.min(dimensions.y, p.y + 1))
                n = n + Cell(Point(x, y))
        n
    }

    private def createLogic(updatesPerSecond: Double): Logic =
        createLogic(new UpdateTimer(updatesPerSecond), activeCells, livingCells)

    private def createLogic(a: Set[Cell], l: Set[Cell]): Logic =
        createLogic(timer, a, l)

    private def createLogic(t: UpdateTimer, a: Set[Cell], l: Set[Cell]) =
        new Logic(t, dimensions, a, l)
}
