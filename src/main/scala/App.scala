import logic.{Logic, Point}
import processing.core.PApplet
import processing.event.MouseEvent

class App extends PApplet {

    private val windowDimensions = Point(400, 400)
    private val gridDimensions   = Point(40, 40)
    private val cellSize = Point(
        windowDimensions.x / gridDimensions.x.toFloat,
        windowDimensions.y / gridDimensions.y.toFloat
    )

    private var logic  = new Logic(5.0f, gridDimensions)
    private var paused = true


    // Starting pattern
    private val centre = Point(gridDimensions.x / 2, gridDimensions.y / 2)
    logic = logic.createCellAt(Point(centre.x,     centre.y))
    logic = logic.createCellAt(Point(centre.x,     centre.y + 1))
    logic = logic.createCellAt(Point(centre.x - 1, centre.y + 1))
    logic = logic.createCellAt(Point(centre.x + 1, centre.y + 1))
    logic = logic.createCellAt(Point(centre.x - 1, centre.y + 2))
    logic = logic.createCellAt(Point(centre.x + 1, centre.y + 2))


    override def settings(): Unit = {
        size(windowDimensions.x, windowDimensions.y)
    }

    override def setup(): Unit = {
        fill(0, 255, 0)
        noStroke()
        background(0)
    }

    override def keyPressed(): Unit =
        key match {
            case ' ' => paused = !paused
            case '=' => logic  = logic.increaseUpdateSpeed()
            case '-' => logic  = logic.decreaseUpdateSpeed()
            case _   =>
        }

    override def mousePressed(e: MouseEvent): Unit = handleMouse(e)
    override def mouseDragged(e: MouseEvent): Unit = handleMouse(e)
    private  def handleMouse(e:  MouseEvent): Unit = {
        logic = e.getButton match {
            case 37 => logic.createCellAt(mouseToGrid(e.getX,   e.getY))
            case 39 => logic.destroyCellAt(mouseToGrid(e.getX, e.getY))
            case _  => logic
        }
    }

    private def mouseToGrid(x: Float, y: Float): Point[Int] =
        Point(
            Math.max(0, Math.min(gridDimensions.x - 1, (x / cellSize.x).toInt)),
            Math.max(0, Math.min(gridDimensions.y - 1, (y / cellSize.y).toInt))
        )

    override def draw(): Unit = {
        if (!paused) logic = logic.update()

        background(0)
        drawCells()
        drawGrid()
    }

    private def drawGrid(): Unit = {
        pushStyle()

        noFill()
        stroke(30)

        for (x <- 0 to gridDimensions.x) line(x * cellSize.x, 0, x * cellSize.x, windowDimensions.y)
        for (y <- 0 to gridDimensions.y) line(0, y * cellSize.y, windowDimensions.x, y * cellSize.y)

        popStyle()
    }

    private def drawCells(): Unit = {
        logic.livingCells.foreach(c => drawCell(c.location))
    }

    private def drawCell(p: Point[Int]): Unit = {
        pushMatrix()
        translate(p.x * cellSize.x, p.y * cellSize.y)
        rect(0, 0, cellSize.x, cellSize.y)
        popMatrix()
    }
}

object App extends PApplet {
    def main(args: Array[String]): Unit = {
        PApplet.main("App")
    }
}
